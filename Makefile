# This file is part of Grot.
# Copyright (C) 2009, 2010 Sergey Poznyakoff
#
# Grot is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Grot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with grot.  If not, see <http://www.gnu.org/licenses/>.

CFLAGS = -O2 -g
CPPFLAGS = -I/usr/include/mysql -I/usr/local/include/mysql
LDFLAGS = -L/usr/lib/mysql -L/usr/local/lib/mysql
LIBS = -lmysqlclient

prefix = /usr
distdir = $(PROJECT)-$(VERSION)
bindir = $(prefix)/bin
mandir = $(prefix)/man

##

PROJECT = grot
VERSION = 1.0

ALLDEFS = -DPACKAGE="\"$(PROJECT)\"" -DVERSION="\"$(VERSION)\""

.c.o:
	$(CC) $(CFLAGS) $(CPPFLAGS) $(ALLDEFS) $(DEFS) -c -o $@ $<

all: grot

grot_SOURCES = \
 grot.c\
 cmdline.c

grot_HEADERS = grot.h

grot_MANS = grot.1 grot.cfg.5

grot_OBJECTS = $(grot_SOURCES:.c=.o)

EXTRA_DIST = cmdline.opt getopt.m4 grot.cfg

grot.o: grot.h grot.c

cmdline.c: cmdline.opt
	m4 -s getopt.m4 $< | sed '1d' > $@

grot: $(grot_OBJECTS)
	$(CC) -ogrot $(grot_OBJECTS) $(LDFLAGS) $(LIBS)

clean:
	rm -f $(grot_OBJECTS) grot

install: install-bin install-man

install-bin: grot
	test -d $(DESTDIR)/$(bindir) || mkdir -p $(DESTDIR)/$(bindir)
	cp grot $(DESTDIR)/$(bindir)

install-man: $(grot_MANS)
	for file in $(grot_MANS); \
	do \
		num=`expr "$$file" : '.*\.\([0-9]\)'`; \
		dir=$(DESTDIR)/$(mandir)/man$$num; \
		test -d $$dir || mkdir -p $$dir; \
		cp $$file $$dir; \
	done

distfiles = \
 AUTHORS\
 COPYING\
 INSTALL\
 README\
 NEWS\
 Makefile\
 $(grot_SOURCES)\
 $(grot_HEADERS)\
 $(grot_MANS)\
 $(EXTRA_DIST)

distdir:
	rm -rf $(distdir) 
	test -d $(distdir) || mkdir -p $(distdir)
	for file in $(distfiles); \
	do \
		cp $$file $(distdir); \
	done

dist:
	make distdir
	tar -c -f - $(distdir) | gzip -c - > $(distdir).tar.gz
	rm -rf $(distdir)

distcheck:
	make dist
	gzip -c -d $(distdir).tar.gz | tar -x -f -
	mkdir $(distdir)/_inst
	cd $(distdir) && make && \
	make install DESTDIR=_inst && \
	make clean
	rm -rf $(distdir)





